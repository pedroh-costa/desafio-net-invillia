﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DN.Emprestimo.Business.Models;

namespace DN.Emprestimo.Business.Interfaces
{
    public interface IAmigoService : IDisposable
    {
        Task<bool> Adicionar(Models.Amigo o);

        Task<bool> Atualizar(Models.Amigo o);

        Task<bool> Excluir(Guid id);

        Task<List<Models.Amigo>> BuscarUserId(Guid UserId);

        Task<Models.Amigo> BuscarId(Guid id);

    }
}
