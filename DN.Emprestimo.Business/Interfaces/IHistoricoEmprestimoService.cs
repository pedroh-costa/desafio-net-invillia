﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DN.Emprestimo.Business.Models;

namespace DN.Emprestimo.Business.Interfaces
{
    public interface IHistoricoEmprestimoService : IDisposable
    {
        Task<bool> Adicionar(Models.HistoricoEmprestimo o);

        Task<bool> Atualizar(Models.HistoricoEmprestimo o);

        Task<bool> Excluir(Guid id);

        Task<List<Models.HistoricoEmprestimo>> BuscarUserId(Guid UserId);

        Task<List<Models.HistoricoEmprestimo>> EmEmprestimo(Guid UserId);

        Task<Models.HistoricoEmprestimo> BuscarId(Guid id);

    }
}
