﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DN.Emprestimo.Business.Models;

namespace DN.Emprestimo.Business.Interfaces
{
    public interface IJogoService : IDisposable
    {
        Task<bool> Adicionar(Models.Jogo o);

        Task<bool> Atualizar(Models.Jogo o);

        Task<bool> Excluir(Guid id);


        Task<List<Models.Jogo>> BuscarUserId(Guid UserId);

       

        Task<bool> EstaEmprestado(Guid Id);

        Task<Models.Jogo> BuscarId(Guid id);

    }
}
