﻿using DN.Emprestimo.Business.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DN.Emprestimo.Business.Interfaces
{
    public interface IJogoRepository : IRepository<Jogo>
    {
        Task<bool> EstaEmprestado(Guid id);
     
    }
}
