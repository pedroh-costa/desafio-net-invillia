﻿using DN.Emprestimo.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DN.Emprestimo.Business.Interfaces
{
    public interface IAmigoRepository : IRepository<Amigo>
    {
    }
}
