﻿using DN.Emprestimo.Business.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DN.Emprestimo.Business.Interfaces
{
    public interface IHistoricoEmprestimoRepository : IRepository<HistoricoEmprestimo>
    {
        Task<List<HistoricoEmprestimo>> EmEmprestimo(Guid UserId);
    }
}
