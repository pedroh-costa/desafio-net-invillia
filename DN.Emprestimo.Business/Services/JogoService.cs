﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DN.Emprestimo.Business.Interfaces;
using DN.Emprestimo.Business.Models;

namespace DN.Emprestimo.Business.Services
{
    public class JogoService : BaseService, IJogoService
    {
        private readonly IJogoRepository _jogoRepository;

        public JogoService(IJogoRepository jogoRepository,
            INotificador notificador) : base(notificador)
        {
            _jogoRepository = jogoRepository;
        }

        public async Task<bool> Adicionar(Models.Jogo o)
        {
            await _jogoRepository.Adicionar(o);

            return true;
        }

        public async Task<bool> Atualizar(Models.Jogo o)
        {
            await _jogoRepository.Atualizar(o);

            return true;
        }

        public async Task<bool> Excluir(Guid id)
        {
            await _jogoRepository.Remover(id);

            return true;
        }

        public async Task<Models.Jogo> BuscarId(Guid id)
        {
            return await _jogoRepository.ObterPorId(id);
        }

        public async Task<List<Models.Jogo>> BuscarUserId(Guid UserId)
        {
            return await _jogoRepository.Buscar((x)=>x.UserId == UserId);
        }

        

        public async Task<bool> EstaEmprestado(Guid Id)
        {
            return await _jogoRepository.EstaEmprestado(Id);
        }

        public void Dispose()
        {
            _jogoRepository?.Dispose();
        }

    }
}
