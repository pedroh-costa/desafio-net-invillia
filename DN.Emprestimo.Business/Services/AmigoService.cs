﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DN.Emprestimo.Business.Interfaces;
using DN.Emprestimo.Business.Models;

namespace DN.Emprestimo.Business.Services
{
    public class AmigoService : BaseService, IAmigoService
    {
        private readonly IAmigoRepository _amigoRepository;

        public AmigoService(IAmigoRepository jogoRepository,
            INotificador notificador) : base(notificador)
        {
            _amigoRepository = jogoRepository;
        }

        public async Task<bool> Adicionar(Models.Amigo o)
        {
            await _amigoRepository.Adicionar(o);

            return true;
        }

        public async Task<bool> Atualizar(Models.Amigo o)
        {
            await _amigoRepository.Atualizar(o);

            return true;
        }

        public async Task<bool> Excluir(Guid id)
        {
            await _amigoRepository.Remover(id);

            return true;
        }

        public async Task<Models.Amigo> BuscarId(Guid id)
        {
            return await _amigoRepository.ObterPorId(id);
        }

        public async Task<List<Models.Amigo>> BuscarUserId(Guid UserId)
        {
            return await _amigoRepository.Buscar(x=>x.UserId == UserId);
        }

        public void Dispose()
        {
            _amigoRepository?.Dispose();
        }

    }
}
