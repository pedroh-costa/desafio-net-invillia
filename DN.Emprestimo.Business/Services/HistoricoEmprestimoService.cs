﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DN.Emprestimo.Business.Interfaces;
using DN.Emprestimo.Business.Models;

namespace DN.Emprestimo.Business.Services
{
    public class HistoricoEmprestimoService : BaseService, IHistoricoEmprestimoService
    {
        private readonly IHistoricoEmprestimoRepository _emprestimoRepository;

        public HistoricoEmprestimoService(IHistoricoEmprestimoRepository emprestimoRepository,
            INotificador notificador) : base(notificador)
        {
            _emprestimoRepository = emprestimoRepository;
        }

        public async Task<bool> Adicionar(Models.HistoricoEmprestimo o)
        {
            await _emprestimoRepository.Adicionar(o);

            return true;
        }

        public async Task<bool> Atualizar(Models.HistoricoEmprestimo o)
        {
            await _emprestimoRepository.Atualizar(o);

            return true;
        }

        public async Task<bool> Excluir(Guid id)
        {
            await _emprestimoRepository.Remover(id);

            return true;
        }

        public async Task<Models.HistoricoEmprestimo> BuscarId(Guid id)
        {
            return await _emprestimoRepository.ObterPorId(id);
        }

        public async Task<List<Models.HistoricoEmprestimo>> BuscarUserId(Guid UserId)
        {
            return await _emprestimoRepository.Buscar(x=>x.UserId == UserId);
        }

        public async Task<List<Models.HistoricoEmprestimo>> EmEmprestimo(Guid UserId)
        {
            return await _emprestimoRepository.EmEmprestimo(UserId);
        }


        public void Dispose()
        {
            _emprestimoRepository?.Dispose();
        }

    }
}
