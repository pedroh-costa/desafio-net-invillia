﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DN.Emprestimo.Business.Models
{
    public class HistoricoEmprestimo : Entity
    {
        
        public DateTime DataEmprestimo { get; set; }
        public DateTime? DataDevolucao { get; set; }

        
        public Guid AmigoId { get; set; }
        public Amigo Amigo { get; set; }

        public Guid JogoId { get; set; }
        public Jogo Jogo { get; set; }

        public Guid UserId { get; set; }

    }

}
