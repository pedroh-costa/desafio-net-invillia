﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DN.Emprestimo.Business.Models
{
    public class Jogo : Entity
    {
        public string Nome { get; set; }
      
        public Guid UserId { get; set; }


        public List<HistoricoEmprestimo> HistoricoEmprestimo { get; set; }

    }
}
