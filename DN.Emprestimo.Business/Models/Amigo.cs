﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DN.Emprestimo.Business.Models
{
    public class Amigo : Entity
    {
        public string Nome { get; set; }
        public string Apelido { get; set; }

        public Guid UserId { get; set; }
    }
}
