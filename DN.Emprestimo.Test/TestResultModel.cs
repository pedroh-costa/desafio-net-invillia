﻿using DN.Emprestimo.API;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;

namespace DN.Emprestimo.Tests
{
    
    public class TestResultModelLogin
    {
        public bool success { get; set; }
        public TestResultModelLoginData data { get; set; }
        public List<string> erro { get; set; }
    }

    public class TestResultModelLoginData
    {
        public string accessToken { get; set; }
    }

   
}
