using DN.Emprestimo.API.ViewModels;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace DN.Emprestimo.Tests
{
    public class UnitTestIntegracao
    {
        private readonly TestContext _testContext;
        public UnitTestIntegracao()
        {
            _testContext = new TestContext();
        } 
        
        [SetUp]
        public void Setup()
        {
            
        }

        private async Task<HttpResponseMessage> Entrar(LoginUserViewModel loginUserViewModel)
        {
            var uriPost = new Uri(_testContext.HttpClient.BaseAddress, "/api/entrar");

            var request = new HttpRequestMessage(HttpMethod.Post, uriPost);

            var myContent = JsonConvert.SerializeObject(loginUserViewModel);

            request.Content = new StringContent(myContent);
            request.Content.Headers.Clear();
            request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            var response = await _testContext.HttpClient.SendAsync(request);

            return response;
        }


        [Test]
        public async Task Entrar_DeveRetornaErro()
        {
            var loginUserViewModel = new LoginUserViewModel() { Email = "teste@example.com", Password = "@teste123" };
            var response = await Entrar(loginUserViewModel);
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }


        [Test]
        public async Task Entrar_DeveRetornaOK()
        {
            var loginUserViewModel = new LoginUserViewModel() { Email = "pedro@example.com", Password = "@aB123" };
            var response = await Entrar(loginUserViewModel);
            string obj = await response.Content.ReadAsStringAsync();
            TestResultModelLogin testResultModelLogin = JsonConvert.DeserializeObject<TestResultModelLogin>(obj);
            
            // Assert
            Assert.NotNull(testResultModelLogin);
            Assert.IsInstanceOf<TestResultModelLogin>(testResultModelLogin);
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }

        
        [Test]
        public async Task AdicionarJogo_DeveRetornaOK()
        {
            var loginUserViewModel = new LoginUserViewModel() { Email = "pedro@example.com", Password = "@aB123" };
            var response = await Entrar(loginUserViewModel);

            string obj = await response.Content.ReadAsStringAsync();
            TestResultModelLogin testResultModelLogin = JsonConvert.DeserializeObject<TestResultModelLogin>(obj);

            if (HttpStatusCode.OK == response.StatusCode)
            {
                var token = testResultModelLogin.data.accessToken;

                var uriPost = new Uri(_testContext.HttpClient.BaseAddress, "/api/jogo/cadastrar");

                var request = new HttpRequestMessage(HttpMethod.Post, uriPost);

                var jogoViewModel = new JogoViewModel() { Nome = "NOVO JOGO" };
                var myContent = JsonConvert.SerializeObject(jogoViewModel);

                request.Content = new StringContent(myContent);
                request.Content.Headers.Clear();
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                request.Headers.Clear();
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

                var responseCadastrar = await _testContext.HttpClient.SendAsync(request);
                responseCadastrar.EnsureSuccessStatusCode();

                // Assert
                Assert.AreEqual(HttpStatusCode.OK, responseCadastrar.StatusCode);
            }
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
    }
}