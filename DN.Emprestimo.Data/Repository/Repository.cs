﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Xml.Schema;
using DN.Emprestimo.Business.Interfaces;
using DN.Emprestimo.Business.Models;
using DN.Emprestimo.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace DN.Emprestimo.Data.Repository
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity, new()
    {
        protected readonly Context.DNDbContext Db;
        protected readonly DbSet<TEntity> DbSet;

        protected Repository(Context.DNDbContext db)
        {
            Db = db;
            DbSet = db.Set<TEntity>();
        }

        public async Task<List<TEntity>> Buscar(Expression<Func<TEntity, bool>> predicate)
        {
            return await DbSet.AsNoTracking().Where(predicate).ToListAsync();
        }

        public virtual async Task<TEntity> ObterPorId(Guid id)
        {
            return await DbSet.AsNoTracking().FirstOrDefaultAsync(x=>x.Id == id);
        }

        public virtual async Task<List<TEntity>> ObterTodos()
        {
            return await DbSet.ToListAsync();
        }

        
        public virtual async Task<PagedResult<TEntity>> ObterTodosPaginado(int pageSize, int pageIndex, string query = null)
        {
            var entity = await DbSet.AsNoTracking().Skip(pageSize*(pageIndex -1)).Take(pageSize).ToListAsync();

            return new PagedResult<TEntity>()
            {
                List = entity,
                TotalResults = entity.Count,
                PageIndex = pageIndex,
                PageSize = pageSize,
                Query = query
            };
        }

        public virtual async Task Adicionar(TEntity entity)
        {
            DbSet.Add(entity);
            await SaveChanges();
        }

        public virtual async Task Atualizar(TEntity entity)
        {
            Db.Entry(entity).State = EntityState.Modified;

            await SaveChanges();
        }

        public virtual async Task Remover(Guid id)
        {
            DbSet.Remove(new TEntity { Id = id });
            await SaveChanges();
        }

        public async Task<int> SaveChanges()
        {
            return await Db.SaveChangesAsync();
        }

        public void Dispose()
        {
            Db?.Dispose();
        }
    }
}
