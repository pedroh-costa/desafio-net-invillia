﻿using System;
using System.Collections.Generic;
using System.Text;
using DN.Emprestimo.Business.Interfaces;
using DN.Emprestimo.Business.Models;
using DN.Emprestimo.Data.Context;

namespace DN.Emprestimo.Data.Repository
{
    public class AmigoRepository : Repository<Amigo>, IAmigoRepository
    {
        public AmigoRepository(DNDbContext context) : base(context) { }
    }
}
