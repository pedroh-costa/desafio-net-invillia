﻿using DN.Emprestimo.Business.Interfaces;
using DN.Emprestimo.Business.Models;
using DN.Emprestimo.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DN.Emprestimo.Data.Repository
{
    public class HistoricoEmprestimoRepository : Repository<HistoricoEmprestimo>, IHistoricoEmprestimoRepository
    {
        public HistoricoEmprestimoRepository(DNDbContext context) : base(context) { }

        public async Task<List<HistoricoEmprestimo>> EmEmprestimo(Guid UserId)
        {
            return await DbSet.
                Include(x => x.Amigo).
                Include(x => x.Jogo).
                AsNoTracking().Where(x => x.UserId == UserId && !x.DataDevolucao.HasValue).ToListAsync();
        }
    }
}
