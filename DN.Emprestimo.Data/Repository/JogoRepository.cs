﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.Emprestimo.Business.Interfaces;
using DN.Emprestimo.Business.Models;
using DN.Emprestimo.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace DN.Emprestimo.Data.Repository
{
    public class JogoRepository : Repository<Jogo>, IJogoRepository
    {
        public JogoRepository(DNDbContext context) : base(context) { }

      

        public async Task<bool> EstaEmprestado(Guid Id)
        {
            var  o = await DbSet.Include(x=>x.HistoricoEmprestimo).AsNoTracking().FirstOrDefaultAsync(x => x.Id == Id);
            if (o == null)
            {
                return false;
            }

            return o.HistoricoEmprestimo.Any(x => !x.DataDevolucao.HasValue);
        }
    }
}
