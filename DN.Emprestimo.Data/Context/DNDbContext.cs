﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DN.Emprestimo.Business.Models;
using Microsoft.EntityFrameworkCore;

namespace DN.Emprestimo.Data.Context
{
    public class DNDbContext : DbContext
    {
        public DNDbContext(DbContextOptions<DNDbContext> options) : base(options) { }

        public DbSet<Amigo> Amigos { get; set; }
        public DbSet<Jogo> Jogos { get; set; }
        public DbSet<HistoricoEmprestimo> HistoricoEmprestimos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes()
                .SelectMany(e => e.GetProperties()
                    .Where(p => p.ClrType == typeof(string))))
                property.SetColumnType("varchar(100)");

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(DNDbContext).Assembly);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;

            base.OnModelCreating(modelBuilder);
        }
    }
}
