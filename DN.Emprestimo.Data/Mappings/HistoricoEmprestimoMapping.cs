﻿using DN.Emprestimo.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DN.Emprestimo.Data.Mappings
{
    public class HistoricoEmprestimoMapping : IEntityTypeConfiguration<HistoricoEmprestimo>
    {
        public void Configure(EntityTypeBuilder<HistoricoEmprestimo> builder)
        {
            builder.HasKey(p => p.Id);

            builder.ToTable("HistoricoEmprestimo");
        }
    }
}
