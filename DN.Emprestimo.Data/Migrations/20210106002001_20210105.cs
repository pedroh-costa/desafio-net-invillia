﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DN.Emprestimo.Data.Migrations
{
    public partial class _20210105 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Quantidade",
                table: "Jogo");

            migrationBuilder.DropColumn(
                name: "Quantidade",
                table: "HistoricoEmprestimo");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Quantidade",
                table: "Jogo",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Quantidade",
                table: "HistoricoEmprestimo",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
