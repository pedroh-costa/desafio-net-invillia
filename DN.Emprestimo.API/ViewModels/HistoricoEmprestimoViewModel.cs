﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DN.Emprestimo.API.ViewModels
{
    public class HistoricoEmprestimoViewModel
    {
        [Key]
        public Guid Id { get; set; }

        [DisplayFormat(DataFormatString = "mm/dd/yyyy")]
        public DateTime DataEmprestimo { get; set; }

        [DisplayFormat(DataFormatString = "mm/dd/yyyy")]
        public DateTime? DataDevolucao { get; set; }

        public Guid AmigoId { get; set; }

        public Guid JogoId { get; set; }
    }


    public class EmEmprestimoViewModel
    {
        [Key]
        public Guid Id { get; set; }

        [DisplayFormat(DataFormatString = "mm/dd/yyyy")]
        public DateTime DataEmprestimo { get; set; }

     
        public Guid AmigoId { get; set; }
        public AmigoViewModel Amigo { get; set; }

        public Guid JogoId { get; set; }
        public JogoViewModel Jogo { get; set; }
    }

}
