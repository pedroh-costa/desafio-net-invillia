﻿using AutoMapper;
using DN.Emprestimo.API.Extensions;
using DN.Emprestimo.API.ViewModels;
using DN.Emprestimo.Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DN.Emprestimo.API.Controllers
{
    [Authorize]
    [Route("api/amigo")]
    public class AmigoController : MainController
    {
        private readonly IAmigoService _amigoService;
        private readonly IMapper _mapper;

        public AmigoController(IAmigoService amigoService,
                                IMapper mapper,
                                INotificador notificador,
                                IUser user) : base(notificador, user)
        {
            _amigoService = amigoService;
            _mapper = mapper;
        }

        [ClaimsAuthorize("administrador", "cadastrar")]
        [HttpPost("cadastrar")]
        public async Task<ActionResult<AmigoViewModel>> Adicionar(AmigoViewModel amigoViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            Guid UserId = AppUser.GetUserId();

            var o = _mapper.Map<Business.Models.Amigo>(amigoViewModel);
            o.UserId = UserId;
            await _amigoService.Adicionar(o);

            return CustomResponse(amigoViewModel);
        }


        
        [HttpPut("edicao/{id:guid}")]
        public async Task<ActionResult<AmigoViewModel>> Atualizar(Guid id, AmigoViewModel amigoViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var obj = await _amigoService.BuscarId(id);

            if (obj == null)
            {
                return NotFound();
            }

            Guid UserId = AppUser.GetUserId();
            if (obj.UserId != UserId)
            {
                return NotFound();
            }
            
            var o = _mapper.Map<Business.Models.Amigo>(amigoViewModel);
            o.UserId = UserId;
            await _amigoService.Atualizar(o);

            return CustomResponse(amigoViewModel);
        }

        [HttpDelete("excluir/{id:guid}")]
        public async Task<ActionResult<AmigoViewModel>> Excluir(Guid id)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var obj = await _amigoService.BuscarId(id);

            if (obj == null)
            {
                return NotFound();
            }
            Guid UserId = AppUser.GetUserId();
            if (obj.UserId != UserId)
            {
                return NotFound();
            }

            await _amigoService.Excluir(id);
            return CustomResponse();
        }

        [HttpGet("listar")]
        public async Task<IEnumerable<AmigoViewModel>> BuscarTodos()
        {
            Guid id = AppUser.GetUserId();
            return _mapper.Map<List<AmigoViewModel>>(await _amigoService.BuscarUserId(id));
        }

    }
}
