﻿using AutoMapper;
using DN.Emprestimo.API.Extensions;
using DN.Emprestimo.API.ViewModels;
using DN.Emprestimo.Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DN.Emprestimo.API.Controllers
{
    [Authorize]
    [Route("api/emprestimo")]
    public class EmprestimoController : MainController
    {
        private readonly IJogoService _jogoService;
        private readonly IAmigoService _amigoService;
        private readonly IHistoricoEmprestimoService _historicoEmprestimoService;
        private readonly IMapper _mapper;

        public EmprestimoController(IHistoricoEmprestimoService historicoEmprestimoService,
                                IJogoService jogoService,
                                IAmigoService amigoService,
                                IMapper mapper,
                                INotificador notificador,
                                IUser user) : base(notificador, user)
        {
            _historicoEmprestimoService = historicoEmprestimoService;
            _jogoService = jogoService;
            _amigoService = amigoService;
            _mapper = mapper;
        }

        [HttpPost("emprestar")]
        public async Task<ActionResult<HistoricoEmprestimoViewModel>> Adicionar(HistoricoEmprestimoViewModel historicoEmprestimoViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);
            Guid UserId = AppUser.GetUserId();

            var objJogo = await _jogoService.BuscarId(historicoEmprestimoViewModel.JogoId);

            if (objJogo == null)
            {
                NotificarErro("Jogo informado inválido.");
                return CustomResponse(historicoEmprestimoViewModel);
            }

            if (objJogo.UserId != UserId)
            {
                NotificarErro("Jogo não pertence ao usuário.");
                return CustomResponse(historicoEmprestimoViewModel);
            }

            var objAmigo = await _amigoService.BuscarId(historicoEmprestimoViewModel.AmigoId);

            if (objAmigo == null)
            {
                NotificarErro("Amigo informado inválido.");
                return CustomResponse(historicoEmprestimoViewModel);
            }

            if (objAmigo.UserId != UserId)
            {
                NotificarErro("Amigo não pertence ao usuário.");
                return CustomResponse(historicoEmprestimoViewModel);
            }

            var EstaEmprestado = await _jogoService.EstaEmprestado(historicoEmprestimoViewModel.JogoId);

            if (EstaEmprestado)
            {
                NotificarErro("Jogo ja está emprestado.");
                return CustomResponse(historicoEmprestimoViewModel);
            }


            var o = _mapper.Map<Business.Models.HistoricoEmprestimo>(historicoEmprestimoViewModel);
            o.UserId = UserId;
            await _historicoEmprestimoService.Adicionar(o);

            return CustomResponse(historicoEmprestimoViewModel);
        }


        
        [HttpPut("devolver/{id:guid}")]
        public async Task<ActionResult<HistoricoEmprestimoViewModel>> Atualizar(Guid id, HistoricoEmprestimoViewModel historicoEmprestimoViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);
            Guid UserId = AppUser.GetUserId();

            var obj = await _historicoEmprestimoService.BuscarId(id);

            if (obj == null)
            {
                return NotFound();
            }

            var objJogo = await _jogoService.BuscarId(historicoEmprestimoViewModel.JogoId);

            if (objJogo == null)
            {
                NotificarErro("Jogo informado inválido.");
                return CustomResponse(historicoEmprestimoViewModel);
            }

            if (objJogo.UserId != UserId)
            {
                NotificarErro("Jogo não pertence ao usuário.");
                return CustomResponse(historicoEmprestimoViewModel);
            }

            var objAmigo = await _amigoService.BuscarId(historicoEmprestimoViewModel.AmigoId);

            if (objAmigo == null)
            {
                NotificarErro("Amigo informado inválido.");
                return CustomResponse(historicoEmprestimoViewModel);
            }

            if (objAmigo.UserId != UserId)
            {
                NotificarErro("Amigo não pertence ao usuário.");
                return CustomResponse(historicoEmprestimoViewModel);
            }

            

            if (obj.UserId != UserId)
            {
                return NotFound();
            }
            
            var o = _mapper.Map<Business.Models.HistoricoEmprestimo>(historicoEmprestimoViewModel);
            o.UserId = UserId;
            await _historicoEmprestimoService.Atualizar(o);

            return CustomResponse(historicoEmprestimoViewModel);
        }

        [HttpDelete("excluir/{id:guid}")]
        public async Task<ActionResult<HistoricoEmprestimoViewModel>> Excluir(Guid id)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var obj = await _historicoEmprestimoService.BuscarId(id);

            if (obj == null)
            {
                return NotFound();
            }
            Guid UserId = AppUser.GetUserId();
            if (obj.UserId != UserId)
            {
                return NotFound();
            }

            await _historicoEmprestimoService.Excluir(id);
            return CustomResponse();
        }

        [HttpGet("listar")]
        public async Task<IEnumerable<HistoricoEmprestimoViewModel>> BuscarTodos()
        {
            Guid id = AppUser.GetUserId();
            return _mapper.Map<List<HistoricoEmprestimoViewModel>>(await _historicoEmprestimoService.BuscarUserId(id));
        }


        [HttpGet("listar-emprestado")]
        public async Task<IEnumerable<EmEmprestimoViewModel>> EmEmprestimo()
        {
            Guid id = AppUser.GetUserId();
            return _mapper.Map<List<EmEmprestimoViewModel>>(await _historicoEmprestimoService.EmEmprestimo(id));
        }


    }
}
