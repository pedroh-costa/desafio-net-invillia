﻿using AutoMapper;
using DN.Emprestimo.API.Extensions;
using DN.Emprestimo.API.ViewModels;
using DN.Emprestimo.Business.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DN.Emprestimo.API.Controllers
{
    [Authorize]
    [Route("api/jogo")]
    public class JogoController : MainController
    {
        private readonly IJogoService _jogoService;
        private readonly IMapper _mapper;

        public JogoController(IJogoService jogoService,
                                IMapper mapper,
                                INotificador notificador,
                                IUser user) : base(notificador, user)
        {
            _jogoService = jogoService;
            _mapper = mapper;
        }

        [ClaimsAuthorize("administrador", "cadastrar")]
        [HttpPost("cadastrar")]
        public async Task<ActionResult<JogoViewModel>> Adicionar(JogoViewModel jogoViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            Guid UserId = AppUser.GetUserId();

            var o = _mapper.Map<Business.Models.Jogo>(jogoViewModel);
            o.UserId = UserId;
            await _jogoService.Adicionar(o);

            return CustomResponse(jogoViewModel);
        }


        
        [HttpPut("edicao/{id:guid}")]
        public async Task<ActionResult<JogoViewModel>> Atualizar(Guid id, JogoViewModel jogoViewModel)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var obj = await _jogoService.BuscarId(id);

            if (obj == null)
            {
                return NotFound();
            }

            Guid UserId = AppUser.GetUserId();
            if (obj.UserId != UserId)
            {
                return NotFound();
            }
            
            var o = _mapper.Map<Business.Models.Jogo>(jogoViewModel);
            o.UserId = UserId;
            await _jogoService.Atualizar(o);

            return CustomResponse(jogoViewModel);
        }

        [HttpDelete("excluir/{id:guid}")]
        public async Task<ActionResult<JogoViewModel>> Excluir(Guid id)
        {
            if (!ModelState.IsValid) return CustomResponse(ModelState);

            var obj = await _jogoService.BuscarId(id);

            if (obj == null)
            {
                return NotFound();
            }
            Guid UserId = AppUser.GetUserId();
            if (obj.UserId != UserId)
            {
                return NotFound();
            }

            await _jogoService.Excluir(id);
            return CustomResponse();
        }

        [HttpGet("listar")]
        public async Task<IEnumerable<JogoViewModel>> BuscarTodos()
        {
            Guid id = AppUser.GetUserId();
            return _mapper.Map<List<JogoViewModel>>(await _jogoService.BuscarUserId(id));
        }

       
    }
}
