﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DN.Emprestimo.API.ViewModels;
using DN.Emprestimo.Business.Models;

namespace DN.Emprestimo.API.Configuration
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<Business.Models.Jogo, JogoViewModel>().ReverseMap();

            CreateMap<Business.Models.Amigo, AmigoViewModel>().ReverseMap();

            CreateMap<Business.Models.HistoricoEmprestimo, HistoricoEmprestimoViewModel>().ReverseMap();

            CreateMap<Business.Models.HistoricoEmprestimo, EmEmprestimoViewModel>().ReverseMap();
        }
    }
}
