﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DN.Emprestimo.API.Extensions;
using DN.Emprestimo.Business.Interfaces;
using DN.Emprestimo.Business.Notificacoes;
using DN.Emprestimo.Business.Services;
using DN.Emprestimo.Data.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using DN.Emprestimo.Data.Repository;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DN.Emprestimo.API.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddScoped<DNDbContext>();
            services.AddScoped<IAmigoRepository, AmigoRepository>();
            services.AddScoped<IJogoRepository, JogoRepository>();
            services.AddScoped<IHistoricoEmprestimoRepository, HistoricoEmprestimoRepository>();

            services.AddScoped<INotificador, Notificador>();
            services.AddScoped<IAmigoService, AmigoService>();
            services.AddScoped<IJogoService, JogoService>();
            services.AddScoped<IHistoricoEmprestimoService, HistoricoEmprestimoService>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUser, AspNetUser>();

            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            return services;
        }
    }
}
